class ResponsesController < ApplicationController
	before_action :authenticate_user!
	def show
		@test = Test.find(params[:id])
		@response = @test.response
		@questions = @test.questions

	end
	
end
