class DashboardController < ApplicationController
	def index
		@test = Test.new
	end

	def create
		@test = Test.new(user_id: current_user.id)
		if @test.save
			@test.questions = Question.order("Random()").limit(5)
			redirect_to test_path(@test)
		end
	end

end
