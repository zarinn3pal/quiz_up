class UsersController < ApplicationController
	before_action :authenticate_user!
	def index
		@users = User.all.where(role: 'normal')
	end
	
	def show
		@tests = User.find(params[:id]).tests.order(created_at: 'desc')

		@max = 0
		@min = 50
		@sum = 0
		@count = 0
		@average = 0
		@tests.each do |test|
			min = 50
			if test.response.score > @max 
				@max = test.response.score
			end
			if test.response.score < @min
				@min = test.response.score
			end
			@sum += test.response.score
			@count += 1
		end
		@sum = @sum.to_f
		@average = @sum/@count
	end
end
