class QuestionsController < ApplicationController
	before_action :authenticate_user!
	def index
		@questions = Question.all.order(created_at: 'desc')
	end

	def new
		@question = Question.new
	end

	def create
		@question = Question.new(question_params)

		if @question.save
			redirect_to questions_path, notice: 'Question Successfully Added'
			
		else
			redirect_to new_question_path
			flash[:danger] = "Question cannot be saved because:#{@question.errors.full_messages}"
		end
	end

	def edit
		@question = Question.find(params[:id])
	end

	def update
		@question = Question.find(params[:id])
		if @question.update(question_params)
			redirect_to questions_path, notice: 'Question Successfully Updated'
		else
			redirect_to edit_question_path(@question)
			flash[:danger] = "Question cannot be Updated because:#{@question.errors.full_messages}"
		end
		
	end
	def destroy
		@question = Question.find(params[:id])
		if @question.destroy
			redirect_to questions_path, notice: 'Question Successfully Deleted'
		end
	end

	private
		def question_params
			params.require(:question).permit(:question, :optiona, :optionb, :optionc, :optiond, :answer)
		end
end
