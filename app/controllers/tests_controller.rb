class TestsController < ApplicationController
	before_action :authenticate_user!
	def show
		@test = Test.find(params[:id])
		@response = Response.new
	end

	def create
		@response = Response.new(response_params)
		if @response.save
			calculated_score = 0 #initial score = 0
			@response_1 = @response[:response_1]
			@response_2 = @response[:response_2]
			@response_3 = @response[:response_3]
			@response_4 = @response[:response_4]
			@response_5 = @response[:response_5]

			num = 0 #array counter
			answer = Array.new
			@response.test.questions.each do |question|
				answer[num] = question.answer
				num += 1
			end

			if @response_1 == answer[0]
				calculated_score+=10
			end
			if @response_2 == answer[1]
				calculated_score+=10
			end
			if @response_3 == answer[2]
				calculated_score+=10
			end
			if @response_4 == answer[3]
				calculated_score+=10
			end
			if @response_5 == answer[4]
				calculated_score+=10
			end

			@response.score = calculated_score
			@response.save
			@response.test.attempted!
			redirect_to root_path, notice: "Your Test Score is #{calculated_score}"
			
		end
		
	end

	private
		def response_params
			params.require(:response).permit(:response_1, :response_2, :response_3, :response_4, :response_5, :test_id)
			
		end
end
