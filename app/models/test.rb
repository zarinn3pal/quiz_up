class Test < ApplicationRecord
	has_and_belongs_to_many :questions
	belongs_to :user
	has_one :response
	enum status: [:unattempted, :attempted]
end
