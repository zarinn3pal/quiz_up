class Question < ApplicationRecord
	validates :question, presence: true
	validates :optiona, presence: true
	validates :optionb, presence: true
	validates :optionc, presence: true
	validates :optiond, presence: true
	validates :answer, presence: true
	has_and_belongs_to_many :tests
end
