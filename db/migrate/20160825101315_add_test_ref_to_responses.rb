class AddTestRefToResponses < ActiveRecord::Migration[5.0]
  def change
    add_reference :responses, :test, foreign_key: true
  end
end
