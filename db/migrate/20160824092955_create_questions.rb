class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.text :question
      t.text :optiona
      t.text :optionb
      t.text :optionc
      t.text :optiond
      t.string :answer

      t.timestamps
    end
  end
end
