class CreateResponses < ActiveRecord::Migration[5.0]
  def change
    create_table :responses do |t|
      t.string :response_1
      t.string :response_2
      t.string :response_3
      t.string :response_4
      t.string :response_5

      t.timestamps
    end
  end
end
