class AddScoreToResponse < ActiveRecord::Migration[5.0]
  def change
    add_column :responses, :score, :integer
  end
end
