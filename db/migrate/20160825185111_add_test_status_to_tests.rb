class AddTestStatusToTests < ActiveRecord::Migration[5.0]
  def change
  	add_column :tests, :status, :integer, default: 0
  end
end
