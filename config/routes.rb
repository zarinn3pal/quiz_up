Rails.application.routes.draw do
  devise_for :users
  resources :questions
  resources :tests, only: [:show, :create]
  resources :responses, only: [:show]
  resources :dashboard, only: [:index, :create]
  resources :users
  root 'dashboard#index'
end
